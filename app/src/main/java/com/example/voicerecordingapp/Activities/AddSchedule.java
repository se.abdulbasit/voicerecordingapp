package com.example.voicerecordingapp.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.voicerecordingapp.BroadcastReciever.AlarmBroadCastReceiver;
import com.example.voicerecordingapp.Model.Word;
import com.example.voicerecordingapp.R;
import com.example.voicerecordingapp.Repository.WordRepository;
import com.example.voicerecordingapp.Utils.Constants;
import com.example.voicerecordingapp.Utils.TimestampConverter;
import com.example.voicerecordingapp.ViewModel.WordViewModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static android.app.PendingIntent.FLAG_CANCEL_CURRENT;

public class AddSchedule extends AppCompatActivity implements View.OnClickListener {
    Calendar date;
    private final String TAG = AddSchedule.class.getName();
    TextView chooseDateAndTime, alarmDuration;
    EditText hasTag, tvName;
    Button btnAdd;
    private WordViewModel mWordViewModel;
    Calendar forReuestCode = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_schedule);
        chooseDateAndTime = findViewById(R.id.chooseDateAndTime);
        alarmDuration = findViewById(R.id.alarmDuration);
        tvName = findViewById(R.id.tvName);
        hasTag = findViewById(R.id.hashTags);
        btnAdd = findViewById(R.id.btnAdd);
        //  date = Calendar.getInstance();
        mWordViewModel = ViewModelProviders.of(this).get(WordViewModel.class);
        chooseDateAndTime.setOnClickListener(this);
        btnAdd.setOnClickListener(this);

    }

    public void showDateTimePicker() {
        final Calendar currentDate = Calendar.getInstance();
        date = Calendar.getInstance();
        new DatePickerDialog(AddSchedule.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date.set(year, monthOfYear, dayOfMonth);
                new TimePickerDialog(AddSchedule.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        date.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        date.set(Calendar.MINUTE, minute);
                        Log.v(TAG, "The choosen one " + date.getTime());

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM dd hh:mm:ss a");
                        String dateStr = simpleDateFormat.format(date.getTime());
                        chooseDateAndTime.setText(/*TimestampConverter.dateToTimestamp(date.getTime())*/ dateStr);
                    }
                }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAdd:
                int duration = 0;
                if (alarmDuration.getText().toString().trim().equals("")) {
                    alarmDuration.setError("Required");
                    return;
                } else if (date == null) {
                    chooseDateAndTime.setError("Required");
                } else {
                    try {
                        duration = Integer.parseInt(alarmDuration.getText().toString());
                        Log.d(TAG, "Duration : " + duration);
                        Log.d(TAG, "Duration : " + Long.valueOf(duration));
                        Word word = new Word(tvName.getText().toString().trim(), date.getTimeInMillis(), Long.valueOf(duration),
                                forReuestCode.getTimeInMillis(), hasTag.getText().toString().trim());
                        mWordViewModel.insert(word);
                        createAlarmReceiver(duration);
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                        alarmDuration.setError("Numbers Only");
                    }
                }
                break;
            case R.id.chooseDateAndTime:
                showDateTimePicker();
                break;
        }
    }

    private void createAlarmReceiver(int duration) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        Log.d(TAG, "Alarm Time : " + date.getTimeInMillis());
        Intent intent = new Intent(this, AlarmBroadCastReceiver.class);
        intent.putExtra(Constants.DURATION, duration);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this,
                100, intent, FLAG_CANCEL_CURRENT);

        alarmManager.set(AlarmManager.RTC_WAKEUP, date.getTimeInMillis(), pendingIntent);
     /*   alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime(),
                AlarmManager.FI *//*3 * 60 * 1000*//*, pendingIntent);*/
    }
}
