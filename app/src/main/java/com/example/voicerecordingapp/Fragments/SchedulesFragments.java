package com.example.voicerecordingapp.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.voicerecordingapp.Activities.AddSchedule;
import com.example.voicerecordingapp.Adapter.MonthListAdapter;
import com.example.voicerecordingapp.Model.DayNoteList;
import com.example.voicerecordingapp.Model.RecycelViewitem;
import com.example.voicerecordingapp.Model.Word;
import com.example.voicerecordingapp.R;
import com.example.voicerecordingapp.Repository.WordRepository;
import com.example.voicerecordingapp.ViewModel.WordViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SchedulesFragments.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SchedulesFragments#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SchedulesFragments extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    WordRepository wordRepository;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    List<Word> wordList;
    RecyclerView recyclerView;
    MonthListAdapter adapter;
    private WordViewModel mWordViewModel;

    public SchedulesFragments() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SchedulesFragments.
     */
    // TODO: Rename and change types and number of parameters
    public static SchedulesFragments newInstance(String param1, String param2) {
        SchedulesFragments fragment = new SchedulesFragments();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
//        wordRepository = new WordRepository(getActivity().getApplicationContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_schedules_fragments, container, false);
        recyclerView = rootView.findViewById(R.id.recycleView);
        adapter = new MonthListAdapter(getContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        // Get a new or existing ViewModel from the ViewModelProvider.
        mWordViewModel = ViewModelProviders.of(this).get(WordViewModel.class);

      /*  FloatingActionButton fab = rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), AddSchedule.class));
            }
        });*/

        // Add an observer on the LiveData returned by getAlphabetizedWords.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        mWordViewModel.getAllWords().observe(this, new Observer<List<Word>>() {
            @Override
            public void onChanged(@Nullable final List<Word> words) {
                // Update the cached copy of the words in the adapter.
                Log.d("ScheduleFragmetn", "Size is :" + words.size());

                List<RecycelViewitem> recycelViewitems = createList(words);
                adapter.setWords(recycelViewitems);

             /*   new Thread(new Runnable() {
                    @Override
                    public void run() {*/
                //     createList(words);
                //    }
                //  });
            }
        });


        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public List<RecycelViewitem> createList(List<Word> wordList) {
        HashSet<Long> monthYearSet = new HashSet<>();
        HashSet<Long> daySet = new HashSet<>();
        Calendar calendar = Calendar.getInstance();
        List<RecycelViewitem> recycelViewitems = new ArrayList<>();

        for (Word word : wordList
        ) {

            calendar.setTimeInMillis(word.getAlarmTime());
            calendar.set(Calendar.MILLISECOND, 0);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            // Adding Days
            daySet.add(calendar.getTimeInMillis());

            // Adding month
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            monthYearSet.add(calendar.getTimeInMillis());

        }

        List<Long> monthYearList = new ArrayList<Long>(monthYearSet);
        List<Long> dayList = new ArrayList<Long>(daySet);

        for (Long month : monthYearList
        ) {
            List<DayNoteList> dayNoteLists = new ArrayList<>();
            calendar.setTimeInMillis(month);
            int year = calendar.get(Calendar.YEAR);
            int monthT = calendar.get(Calendar.MONTH);

            for (Long day : dayList) {
                List<Word> wordsList2 = new ArrayList<>();
                calendar.setTimeInMillis(day);
                wordsList2.clear();
                int dayT = calendar.get(Calendar.DAY_OF_MONTH);

                // Create a list against those month and day
                for (Word word : wordList
                ) {
                    calendar.setTimeInMillis(word.getAlarmTime());
                    if (calendar.get(Calendar.YEAR) == year
                            && calendar.get(Calendar.MONTH) == monthT
                            && calendar.get(Calendar.DAY_OF_MONTH) == dayT) {
                        wordsList2.add(word);
                    }

                }
                if (wordsList2.size() > 0) {
                    DayNoteList dayNoteList = new DayNoteList(day, wordsList2);
                    dayNoteLists.add(dayNoteList);
                }
            }
            if (dayNoteLists.size() > 0)
                recycelViewitems.add(new RecycelViewitem(month, dayNoteLists));


        }
        return recycelViewitems;

        // System.out.println("List Size : " + recycelViewitems.size());

    }
}
