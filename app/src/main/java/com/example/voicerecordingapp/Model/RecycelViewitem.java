package com.example.voicerecordingapp.Model;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by Abdul-Basit on 22/07/2019.
 */
public class RecycelViewitem {
    Long month;
    List<DayNoteList> dayNoteLists = new ArrayList<>();

    public RecycelViewitem(Long month, List<DayNoteList> dayNoteLists) {
        this.dayNoteLists = dayNoteLists;
        this.month = month;
    }


    public void setDayNoteLists(List<DayNoteList> dayNoteLists) {
        this.dayNoteLists = dayNoteLists;
    }

    public List<DayNoteList> getDayNoteLists() {
        return dayNoteLists;
    }

    public Long getMonth() {
        return month;
    }

    public void setMonth(Long month) {
        this.month = month;
    }
}
