package com.example.voicerecordingapp.Model;

public class RecordingModel {

    String Name;
    String Tag;
    String DateTime;
    String Bytes;
    private boolean isFolder;
    private boolean isFavourt;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getTag() {
        return Tag;
    }

    public void setTag(String tag) {
        Tag = tag;
    }

    public String getDateTime() {
        return DateTime;
    }

    public void setDateTime(String dateTime) {
        DateTime = dateTime;
    }

    public String getBytes() {
        return Bytes;
    }

    public void setBytes(String bytes) {
        Bytes = bytes;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    String Duration;

    public boolean isFolder() {
        return isFolder;
    }

    public void setFolder(boolean folder) {
        isFolder = folder;
    }

    public boolean isFavourt() {
        return isFavourt;
    }

    public void setFavourt(boolean favourt) {
        isFavourt = favourt;
    }
}
