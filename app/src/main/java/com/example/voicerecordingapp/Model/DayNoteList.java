package com.example.voicerecordingapp.Model;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by Abdul-Basit on 22/07/2019.
 */
public class DayNoteList {
    Long day;
    List<Word> noteList = new ArrayList<>();

    public Long getDay() {
        return day;
    }

    public DayNoteList(Long day, List<Word> noteList) {
        this.day = day;
        this.noteList = noteList;
    }

    public void setDay(Long day) {
        this.day = day;
    }

    public List<Word> getNoteList() {
        return noteList;
    }

    public void setNoteList(List<Word> noteList) {
        this.noteList = noteList;
    }
}
