package com.example.voicerecordingapp.Model;

/*
 * Created by Abdul-Basit on 13/07/2019.
 */

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;
import androidx.room.TypeConverters;

import com.example.voicerecordingapp.Utils.TimestampConverter;

import java.util.Calendar;
import java.util.Date;

/**
 * A basic class representing an entity that is a row in a one-column database table.
 *
 * @ Entity - You must annotate the class as an entity and supply a table name if not class name.
 * @ PrimaryKey - You must identify the primary key.
 * @ ColumnInfo - You must supply the column name if it is different from the variable name.
 * <p>
 * See the documentation for the full rich set of annotations.
 * https://developer.android.com/topic/libraries/architecture/room.html
 */

@Entity(tableName = "word_table")
public class Word {

    @PrimaryKey(autoGenerate = true)
    private int uid;
    @ColumnInfo(name = "text")
    private String word;

    @ColumnInfo(name = "created_at")
    // @TypeConverters({TimestampConverter.class})
    private Long createdAt;

    @ColumnInfo(name = "modified_at")
    //  @TypeConverters({TimestampConverter.class})
    private Long modifiedAt;

    @ColumnInfo(name = "alarm_time")
    private Long alarmTime;

    @ColumnInfo(name = "alarm_duration")
    private Long alarmDuration;

    @ColumnInfo(name = "request_code")
    private Long requestCode;

    @ColumnInfo(name = "hash_tag")
    private String hashTag;

    public Word(){}

    public Word(String mWord, Long alarmTime, Long alarmDuration, Long requestCode, String hashTag) {
        Calendar calendar = Calendar.getInstance();
        this.word = mWord;
        this.createdAt = calendar.getTimeInMillis();
        this.modifiedAt = calendar.getTimeInMillis();
        this.alarmTime = alarmTime;
        this.alarmDuration = alarmDuration;
        this.requestCode = requestCode;
        this.hashTag = hashTag;

    }

    public Long getRequestCode() {
        return requestCode;
    }

    public String getHashTag() {
        return hashTag;
    }

    public void setHashTag(String hashTag) {
        this.hashTag = hashTag;
    }

    public void setRequestCode(Long requestCode) {
        this.requestCode = requestCode;
    }

    public int getUid() {
        return uid;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public Long getAlarmTime() {
        return alarmTime;
    }

    public Long getAlarmDuration() {
        return alarmDuration;
    }

    public void setAlarmTime(Long alarmTime) {
        this.alarmTime = alarmTime;
    }

    public void setAlarmDuration(Long alarmDuration) {
        this.alarmDuration = alarmDuration;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public Long getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(Long modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

}
