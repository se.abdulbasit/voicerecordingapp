package com.example.voicerecordingapp.BroadcastReciever;

/*
 * Created by Abdul-Basit on 14/07/2019.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.voicerecordingapp.Services.myservice;
import com.example.voicerecordingapp.Utils.Constants;

public class AlarmBroadCastReceiver extends BroadcastReceiver {
    String TAG = "AlarmReceiver";
    int duration = 1;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            Log.d(TAG, "Intent action:: " + intent.getAction());

            if (intent.hasExtra("duration")) {
                duration = intent.getIntExtra(Constants.DURATION, 1);
            }

            Intent i = new Intent(context, myservice.class);
            i.putExtra(Constants.DURATION, duration);
            context.startService(i);
/*
            Intent intent1 = new Intent();
            intent1.setAction("JOB INTENT SERVICE");
            AlarmJobIntentService.enqueueWork(context, AlarmJobIntentService.class, 1, intent1);


            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent intent2 = new Intent(context, AlarmBroadCastReceiver.class);
            intent.setAction("android.alarm.receiver");
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                    101, intent2, FLAG_CANCEL_CURRENT);

            // For one Minute
            //alarmManager.set(AlarmManager.RTC_WAKEUP,System.currentTimeMillis()+10*1000,pendingIntent);
            alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    SystemClock.elapsedRealtime() +
                            60 * 1000, pendingIntent);*/

        }
    }
}
