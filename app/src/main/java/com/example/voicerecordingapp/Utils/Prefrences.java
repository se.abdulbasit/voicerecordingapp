package com.example.voicerecordingapp.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.voicerecordingapp.Model.RecordingModel;
import com.google.gson.Gson;

import java.util.ArrayList;

public class Prefrences {

    static String Recording = "REC";
    static String Folder = "FLD";
    public  static void  putRecordingString(Context context , String value )
    {
        SharedPreferences preferences = context.getSharedPreferences(context.getPackageName(), Activity.MODE_PRIVATE);
        String newValue = getALLRecordingString(context);
        if(newValue == null)
            newValue = value;
        else
            newValue = newValue +"&&&"+value;

        SharedPreferences.Editor prefsEditor = preferences.edit();
        prefsEditor.putString(Recording, newValue);
        prefsEditor.apply();
        Log.e("test" , " " +newValue);


    }
    public static String getALLRecordingString(Context context)
    {
        SharedPreferences preferences = context.getSharedPreferences(context.getPackageName(), Activity.MODE_PRIVATE);
        return preferences.getString(Recording , null);


    }
    public static ArrayList<RecordingModel> getRecordingList(Context context)
    {
        ArrayList<RecordingModel> list =  new ArrayList<RecordingModel>();
        String k = getALLRecordingString(context);
        if(k == null)
            return list;
        String []values = k.split("&&&");
        Gson gson =  new Gson();
        for(int  i=0 ; i<values.length ; i++)
        {
            RecordingModel model = gson.fromJson(values[i], RecordingModel.class);
            list.add(model);

        }
        return list;

    }

}
