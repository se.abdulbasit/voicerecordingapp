package com.example.voicerecordingapp.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.voicerecordingapp.Model.RecordingModel;
import com.example.voicerecordingapp.R;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.example.voicerecordingapp.Fragments.HomeFragment.setAdapter;

public class CommonMethods {

    public static void showToast(Context context , String text)
    {
        Toast.makeText(context , text , Toast.LENGTH_SHORT).show();
    }
    public static void showNewDialog(Context context , boolean isFolder)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(R.layout.activity_add_schedule);
        builder.setCancelable(false);
        AlertDialog dialog = builder.show();
        Button btnCancel = dialog.findViewById(R.id.btnCancel);
        btnCancel.setVisibility(View.VISIBLE);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Button btnAdd = dialog.findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tag , name;
                EditText hashTags = dialog.findViewById(R.id.hashTags);
                tag = hashTags.getText().toString();
                EditText tvName = dialog.findViewById(R.id.tvName);
                name = tvName.getText().toString();
                RecordingModel recordingModel = new RecordingModel();
                recordingModel.setName(name);
                recordingModel.setTag(tag);
                recordingModel.setFavourt(false);
                recordingModel.setFolder(isFolder);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd");
                recordingModel.setDateTime(simpleDateFormat.format(Calendar.getInstance().getTime()));
                recordingModel.setBytes("89KB");
                recordingModel.setDuration("00:00:09");
                Gson gson = new Gson();
                String value = gson.toJson(recordingModel);
                Prefrences.putRecordingString(context , value);
                setAdapter();
                dialog.dismiss();
            }
        });

        EditText alarmDuration = dialog.findViewById(R.id.alarmDuration);
        alarmDuration.setVisibility(View.GONE);
        TextView chooseDateAndTime = dialog.findViewById(R.id.chooseDateAndTime);
        chooseDateAndTime.setVisibility(View.GONE);

    }

    public static void showMenuDialog(Context context , String[] option)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(option, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        builder.show();
    }
}
