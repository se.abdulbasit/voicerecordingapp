package com.example.voicerecordingapp.Utils;

import androidx.room.TypeConverter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/*
 * Created by Abdul-Basit on 13/07/2019.
 */
public class TimestampConverter {

    private static DateFormat df = new SimpleDateFormat(/*yyyy-MM-dd*/"HH:mm a");

    public static Date fromTimestamp(String value) {
        if (value != null) {
            try {
                //    TimeZone timeZone = TimeZone.getTimeZone("IST");
                //     df.setTimeZone(timeZone);
                return df.parse(value);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return null;
        } else {
            return null;
        }
    }


    public static String dateToTimestamp(Date value) {
        //  TimeZone timeZone = TimeZone.getTimeZone("IST");
        //  df.setTimeZone(timeZone);
        return value == null ? null : df.format(value);
    }
}
