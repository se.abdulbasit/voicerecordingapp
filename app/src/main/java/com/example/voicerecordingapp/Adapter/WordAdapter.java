package com.example.voicerecordingapp.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.voicerecordingapp.Model.DayNoteList;
import com.example.voicerecordingapp.Model.Word;
import com.example.voicerecordingapp.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/*
 * Created by Abdul-Basit on 24/07/2019.
 */
public class WordAdapter extends RecyclerView.Adapter<WordAdapter.WordViewHolder> {
    private final String TAG = WordAdapter.class.getName();

    class WordViewHolder extends RecyclerView.ViewHolder {
        private TextView alarmTime, alarmDuration, tvName, tvHashTags;
        CardView cardView;
        RelativeLayout relativeLayout;
        View smallView, lineView;

        private WordViewHolder(View itemView) {
            super(itemView);
            alarmDuration = itemView.findViewById(R.id.alarmDuration);
            alarmTime = itemView.findViewById(R.id.alarmTime);
            cardView = itemView.findViewById(R.id.cardView);
            relativeLayout = itemView.findViewById(R.id.relativeLayout);
            tvName = itemView.findViewById(R.id.name);
            tvHashTags = itemView.findViewById(R.id.hashTags);
            smallView = itemView.findViewById(R.id.smallView);
            lineView = itemView.findViewById(R.id.lineView);

        }
    }

    private final LayoutInflater mInflater;
    private List<Word> mWords = Collections.emptyList(); // Cached copy of words

    public WordAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public WordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.word_item, parent, false);
        return new WordViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(WordViewHolder holder, int position) {
        Word current = mWords.get(position);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(current.getAlarmTime());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
        String date = simpleDateFormat.format(calendar.getTime());
        holder.alarmTime.setText(date);
        holder.alarmDuration.setText(String.valueOf(current.getAlarmDuration()));
        holder.tvName.setText(String.valueOf(current.getWord()));
        holder.tvHashTags.setText(String.valueOf(current.getHashTag()));
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        // holder.cardView.setBackgroundColor(color);
        holder.relativeLayout.setBackgroundColor(color);
        if (current.getHashTag().equals("") && current.getWord().equals("")) {
            holder.lineView.setVisibility(View.GONE);
            holder.smallView.setVisibility(View.GONE);
            holder.tvName.setVisibility(View.GONE);
            holder.tvHashTags.setVisibility(View.GONE);
        }


    }

    public void setWords(List<Word> words) {
        mWords = words;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mWords.size();
    }
}
