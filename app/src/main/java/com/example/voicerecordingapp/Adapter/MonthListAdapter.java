package com.example.voicerecordingapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.voicerecordingapp.Model.RecycelViewitem;
import com.example.voicerecordingapp.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

/*
 * Created by Abdul-Basit on 14/07/2019.
 */
public class MonthListAdapter extends RecyclerView.Adapter<MonthListAdapter.WordViewHolder> {
    private final String TAG = MonthListAdapter.class.getName();
    Context context;

    class WordViewHolder extends RecyclerView.ViewHolder {
        private final TextView textViewTime, textViewDuration;
        RecyclerView dayRecycleView;

        private WordViewHolder(View itemView) {
            super(itemView);
            textViewTime = itemView.findViewById(R.id.time);
            textViewDuration = itemView.findViewById(R.id.duration);
            dayRecycleView = itemView.findViewById(R.id.dayRecycleView);

        }
    }

    private final LayoutInflater mInflater;
    private List<RecycelViewitem> mWords = Collections.emptyList(); // Cached copy of words

    public MonthListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        this.context=context;
    }

    @Override
    public WordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new WordViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(WordViewHolder holder, int position) {
        RecycelViewitem current = mWords.get(position);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(current.getMonth());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM yyyy");
        String date = simpleDateFormat.format(calendar.getTime());
        System.out.println(date);
        holder.textViewDuration.setText(date);
        DayAdapter dayAdapter=new DayAdapter(context);
        dayAdapter.setWords(current.getDayNoteLists());

        holder.dayRecycleView.setAdapter(dayAdapter);

    }

    public void setWords(List<RecycelViewitem> words) {
        mWords = words;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mWords.size();
    }
}


