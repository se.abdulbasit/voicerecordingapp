package com.example.voicerecordingapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.voicerecordingapp.Model.DayNoteList;
import com.example.voicerecordingapp.Model.RecycelViewitem;
import com.example.voicerecordingapp.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/*
 * Created by Abdul-Basit on 24/07/2019.
 */
public class DayAdapter extends RecyclerView.Adapter<DayAdapter.WordViewHolder> {
    private final String TAG = DayAdapter.class.getName();
    Context context;

    class WordViewHolder extends RecyclerView.ViewHolder {
        private TextView currentDate, currentDay;
        RecyclerView recycleView;

        private WordViewHolder(View itemView) {
            super(itemView);
            currentDate = itemView.findViewById(R.id.currentDate);
            currentDay = itemView.findViewById(R.id.currentDay);
            recycleView = itemView.findViewById(R.id.recycleView2);

        }
    }

    private final LayoutInflater mInflater;
    private List<DayNoteList> mWords = Collections.emptyList(); // Cached copy of words

    public DayAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public WordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.day_item, parent, false);
        return new WordViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(WordViewHolder holder, int position) {
        DayNoteList current = mWords.get(position);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(current.getDay());
        String dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(calendar.getTime());
        System.out.println(dayOfWeek);
        holder.currentDate.setText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
        holder.currentDay.setText(dayOfWeek);
        WordAdapter wordAdapter = new WordAdapter(context);
        holder.recycleView.setAdapter(wordAdapter);
        wordAdapter.setWords(current.getNoteList());

    }

    public void setWords(List<DayNoteList> words) {
        mWords = words;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mWords.size();
    }
}