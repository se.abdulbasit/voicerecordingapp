package com.example.voicerecordingapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.voicerecordingapp.Model.RecordingModel;
import com.example.voicerecordingapp.R;
import com.example.voicerecordingapp.Utils.CommonMethods;

import java.util.ArrayList;

public class RecordingAdapter extends BaseAdapter {

    Context context;
    ArrayList<RecordingModel> List;

    public RecordingAdapter(Context context , ArrayList<RecordingModel> list)
    {
        this.context = context;
        this.List = list;

    }

    @Override
    public int getCount() {
        return List.size();
    }

    @Override
    public Object getItem(int position) {
        return List.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recording_item, parent, false);
        RecordingModel model = List.get(position);
        TextView name = view.findViewById(R.id.name);
        name.setText(model.getName());
        TextView dur = view.findViewById(R.id.dur);
        dur.setText(model.getDuration());
        TextView byt = view.findViewById(R.id.byt);
        byt.setText(model.getBytes());
        ImageView menu = view.findViewById(R.id.menu);
        ImageView fav = view.findViewById(R.id.fav);
        ImageView icon = view.findViewById(R.id.icon);
        LinearLayout recThing = view.findViewById(R.id.recThing);
        if(model.isFolder())
        {
            recThing.setVisibility(View.GONE);
            icon.setImageResource(R.drawable.ic_folder_black_24dp);
        }
        else
        {
            recThing.setVisibility(View.VISIBLE);
            icon.setImageResource(R.drawable.play);
        }
        if(model.isFavourt())
            fav.setImageResource(R.drawable.favourate);
        else
            fav.setImageResource(R.drawable.unfavourt);

        fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                model.setFavourt(!model.isFavourt());
                if(model.isFavourt())
                    fav.setImageResource(R.drawable.favourate);
                else
                    fav.setImageResource(R.drawable.unfavourt);
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String[] options;
                if(model.isFolder())
                {
                    options = new String[]{"Rename","Delete","Paste"};
                }
                else
                {
                    options = new String[]{"Share","Rename","Move","Copy","Delete"};
                }
                CommonMethods.showMenuDialog(context , options);
            }
        });


        return view;
    }
}
